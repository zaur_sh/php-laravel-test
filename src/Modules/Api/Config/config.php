<?php

return [
    'client' => [
        'base_uri' => env('API_CLIENT_BASE_URI', 'localhost'),
        'timeout' => env('API_CLIENT_TIMEOUT', 30),
    ],
];
