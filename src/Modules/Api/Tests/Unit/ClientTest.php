<?php
declare(strict_types=1);

namespace Modules\Api\Tests\Unit;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Modules\Api\Client;
use Psr\Log\NullLogger;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use Tests\TestCase;

class ClientTest extends TestCase
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__.'/ClientTestFixture1.txt')),
            new Response(200, [], file_get_contents(__DIR__.'/ClientTestFixture2.txt')),
            new Response(200, [], file_get_contents(__DIR__.'/ClientTestFixture3.txt')),
        ]);
        $httpClient = new HttpClient(['handler' => HandlerStack::create($mock)]);
        $this->client = new Client($httpClient, new NullLogger());
    }

    /**
     * @throws UnknownProperties
     * @throws GuzzleException
     */
    public function testLimitItems()
    {
        $response = $this->client->getCargos(limitItems: 3);
        $this->assertEquals(3, $response->count());
    }

    /**
     * @throws UnknownProperties
     * @throws GuzzleException
     */
    public function testLimitPages()
    {
        $response = $this->client->getCargos(limitPages: 1);
        $this->assertEquals(2, $response->count());
    }

    /**
     * @throws UnknownProperties
     * @throws GuzzleException
     */
    public function testGetAll()
    {
        $response = $this->client->getCargos();
        $this->assertEquals(4, $response->count());
    }
}
