<?php
declare(strict_types=1);

namespace Modules\Api\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Spatie\DataTransferObject\DataTransferObject;

class CargoDTO extends DataTransferObject implements Arrayable
{
    public int $id = 0;
    public int $weight = 0;
    public int $volume = 0;
    public TruckDTO $truck;
}
