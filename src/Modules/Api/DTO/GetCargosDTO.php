<?php
declare(strict_types=1);

namespace Modules\Api\DTO;

use Illuminate\Support\Collection;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;

class GetCargosDTO extends DataTransferObject
{
    #[CastWith(CargoCollectionCaster::class)]
    public Collection $data;
}
