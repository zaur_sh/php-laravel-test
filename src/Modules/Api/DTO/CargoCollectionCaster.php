<?php
declare(strict_types=1);

namespace Modules\Api\DTO;

use Illuminate\Support\Collection;
use Spatie\DataTransferObject\Caster;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class CargoCollectionCaster implements Caster
{
    public function cast(mixed $value): Collection
    {
        return new Collection(array_map(
        /**
         * @throws UnknownProperties
         */ fn (array $data) => new CargoDTO(...$data),
            $value
        ));
    }
}
