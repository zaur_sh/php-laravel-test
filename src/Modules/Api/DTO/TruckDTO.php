<?php
declare(strict_types=1);

namespace Modules\Api\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Spatie\DataTransferObject\DataTransferObject;

class TruckDTO extends DataTransferObject implements Arrayable
{
    public int $quantity = 0;
    public int $belt_count = 0;
    public int $place_count = 0;
    public int $pallet_count = 0;
}
