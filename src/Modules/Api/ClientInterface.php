<?php
declare(strict_types=1);

namespace Modules\Api;

use Illuminate\Support\Collection;
use Modules\Api\DTO\CargoDTO;

interface ClientInterface
{
    const DEFAULT_PER_PAGE = 100;

    function getCargo(int $id): CargoDTO;

    function getCargos(
        int $perPage = self::DEFAULT_PER_PAGE,
        int $limitPages = PHP_INT_MAX,
        int $limitItems = PHP_INT_MAX
    ): Collection;
}
