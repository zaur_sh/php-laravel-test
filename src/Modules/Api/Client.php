<?php
declare(strict_types=1);

namespace Modules\Api;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Utils;
use Illuminate\Support\Collection;
use Modules\Api\DTO\CargoDTO;
use Modules\Api\DTO\GetCargosDTO;
use Psr\Log\LoggerInterface;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class Client implements ClientInterface
{
    public function __construct(
        private \GuzzleHttp\Client $httpClient,
        private LoggerInterface $logger
    )
    {}

    /**
     * @throws UnknownProperties
     * @throws GuzzleException
     */
    function getCargo(int $id): CargoDTO
    {
        $response = $this->httpClient->get("/v1/cargos/{$id}");
        $body = $response->getBody()->getContents();

        $this->logger->debug(__CLASS__, ['headers' => $response->getHeaders(), 'body' => $body]);

        return new CargoDTO(Utils::jsonDecode($body, true));
    }

    /**
     * @throws UnknownProperties
     * @throws GuzzleException
     */
    private function getCargosChunk(int $page, int $perPage): GetCargosDTO
    {
        $response = $this->httpClient->get('/v1/cargos?'.http_build_query([
            'offset' => $perPage * ($page - 1),
            'limit' => $perPage,
        ]));
        $body = $response->getBody()->getContents();

        $this->logger->debug(__CLASS__, ['headers' => $response->getHeaders(), 'body' => $body]);

        return new GetCargosDTO(Utils::jsonDecode($body, true));
    }

    /**
     * @throws UnknownProperties
     * @throws GuzzleException
     */
    function getCargos(
        int $perPage = self::DEFAULT_PER_PAGE,
        int $limitPages = PHP_INT_MAX,
        int $limitItems = PHP_INT_MAX
    ): Collection
    {
        $cargoCollection = new Collection();
        for ($page = 1; $page <= $limitPages; $page++) {
            $chunk = $this->getCargosChunk($page, $perPage);

            if ($chunk->data->isEmpty()) {
                return $cargoCollection;
            }

            foreach ($chunk->data as $cargo) {
                if ($cargoCollection->count() >= $limitItems) {
                    return $cargoCollection;
                }

                $cargoCollection->add($cargo);
            }
        }

        return $cargoCollection;
    }
}
