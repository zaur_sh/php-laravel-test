<?php
declare(strict_types=1);

namespace Modules\Api\Providers;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Modules\Api\Client;
use Modules\Api\ClientInterface;

class ApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('api.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'api'
        );
    }

    public function register()
    {
        $this->app->singleton(ClientInterface::class, function (Application $app) {
            return new Client(new \GuzzleHttp\Client(config('api.client')), $app['log']);
        });
    }

    public function provides(): array
    {
        return [ClientInterface::class];
    }
}
