<?php
declare(strict_types=1);

namespace Modules\Cargo\Console;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Modules\Api\ClientInterface;
use Modules\Cargo\Jobs\CargoSyncJob;

class SyncCommand extends Command
{
    use DispatchesJobs;

    protected $signature = 'cargo:sync {--limit_pages=10}';
    protected $description = 'Synchronizes cargo data.';

    public function handle(ClientInterface $client): void
    {
        $limitPages = $this->option('limit_pages');

        if (!isset($limitPages)) {
            $limitPages = 10;
        }

        if (!is_numeric($limitPages)) {
            $this->error('limit_pages is not numeric');
            return;
        }

        $cargos = $client->getCargos(limitPages: (int) $limitPages);
        foreach ($cargos as $cargoDTO) {
            $this->dispatch(new CargoSyncJob($cargoDTO));
        }
    }
}
