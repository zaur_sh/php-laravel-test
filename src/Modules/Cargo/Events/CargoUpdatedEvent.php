<?php
declare(strict_types=1);

namespace Modules\Cargo\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Cargo\Models\Cargo;

class CargoUpdatedEvent
{
    use SerializesModels;

    public Cargo $cargo;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Cargo $cargo)
    {
        $this->cargo = $cargo;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn(): array
    {
        return [];
    }
}
