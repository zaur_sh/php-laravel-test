<?php

namespace Modules\Cargo\Casts;

use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Modules\Api\DTO\TruckDTO;

class AsTruckDTO implements Castable
{
    public static function castUsing(array $arguments): CastsAttributes
    {
        return new class implements CastsAttributes {

            public function get($model, string $key, $value, array $attributes): ?TruckDTO
            {
                return isset($attributes[$key]) ? new TruckDTO(json_decode($attributes[$key], true)) : null;
            }

            public function set($model, string $key, $value, array $attributes): array
            {
                return [$key => json_encode($value)];
            }
        };
    }
}
