<?php

use Modules\Cargo\Http\Controllers\CargoController;

Route::apiResource('cargos', CargoController::class);
