<?php
declare(strict_types=1);

namespace Modules\Cargo\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Api\DTO\CargoDTO;
use Modules\Cargo\Models\Cargo;

class CargoSyncJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private CargoDTO $cargoDTO;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CargoDTO $cargoDTO)
    {
        $this->cargoDTO = $cargoDTO;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Cargo::query()->updateOrCreate([Cargo::ID => $this->cargoDTO->id], $this->cargoDTO->toArray());
    }
}
