<?php
declare(strict_types=1);

namespace Modules\Cargo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetCargosRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'offset' => 'number',
            'limit' => 'number|max:100',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function getOffset(): int
    {
        return $this->input('offset', 0);
    }

    public function getLimit(): int
    {
        return $this->input('limit', 10);
    }
}
