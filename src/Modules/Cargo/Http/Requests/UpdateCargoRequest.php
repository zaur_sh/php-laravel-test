<?php
declare(strict_types=1);

namespace Modules\Cargo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Cargo\Models\Cargo;

class UpdateCargoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Cargo::VOLUME => 'numeric',
            Cargo::WEIGHT => 'numeric',
            Cargo::TRUCK.'.quantity' => 'numeric',
            Cargo::TRUCK.'.belt_count' => 'numeric',
            Cargo::TRUCK.'.place_count' => 'numeric',
            Cargo::TRUCK.'.pallet_count' => 'numeric',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
