<?php
declare(strict_types=1);

namespace Modules\Cargo\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Cargo\Http\Requests\GetCargosRequest;
use Modules\Cargo\Http\Requests\StoreCargoRequest;
use Modules\Cargo\Http\Requests\UpdateCargoRequest;
use Modules\Cargo\Models\Cargo;
use Modules\Cargo\Transformers\CargoResource;
use Modules\Cargo\Transformers\CargoResourceCollection;

class CargoController extends Controller
{
    public function index(GetCargosRequest $cargosRequest): JsonResponse
    {
        $list = Cargo::getListFromRequest($cargosRequest);
        return response()->json([
            'meta' => [
                'offset' => $cargosRequest->getOffset(),
                'limit' => $cargosRequest->getLimit(),
            ],
            'data' => new CargoResourceCollection($list),
        ]);
    }

    public function store(StoreCargoRequest $request): JsonResponse
    {
        $cargo = new Cargo($request->all());
        $cargo->save();
        return response()->json([
            'data' => new CargoResource($cargo),
        ]);
    }

    public function show(Cargo $cargo): JsonResponse
    {
        return response()->json([
            'data' => new CargoResource($cargo),
        ]);
    }

    public function update(UpdateCargoRequest $request, Cargo $cargo): JsonResponse
    {
        $cargo->update($request->all());
        return response()->json([
            'data' => new CargoResource($cargo),
        ]);
    }
}
