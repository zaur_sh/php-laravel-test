<?php
declare(strict_types=1);

namespace Modules\Cargo\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Modules\Api\DTO\CargoDTO;
use Modules\Cargo\Casts\AsTruckDTO;
use Modules\Cargo\Database\factories\CargoFactory;
use Modules\Cargo\Events\CargoCreatedEvent;
use Modules\Cargo\Events\CargoUpdatedEvent;
use Modules\Cargo\Http\Requests\GetCargosRequest;

class Cargo extends Model
{
    use HasFactory, SoftDeletes;

    public $incrementing = false;

    const ID = 'id';
    const WEIGHT = 'weight';
    const VOLUME = 'volume';
    const TRUCK = 'truck';
    const DELETED_AT = 'deleted_at';

    protected $fillable = [
        self::ID,
        self::WEIGHT,
        self::VOLUME,
        self::TRUCK,
    ];

    protected $casts = [
        self::TRUCK => AsTruckDTO::class,
    ];

    protected $dispatchesEvents = [
        'created' => CargoCreatedEvent::class,
        'updated' => CargoUpdatedEvent::class,
    ];

    public static function newFromDTO(CargoDTO $cargoDTO): Cargo
    {
        return new Cargo($cargoDTO->toArray());
    }

    public static function getListFromRequest(GetCargosRequest $request): Collection
    {
        return Cargo::query()->offset($request->getOffset())->limit($request->getLimit())->get();
    }

    protected static function newFactory(): CargoFactory
    {
        return CargoFactory::new();
    }

    public function setIdAttribute(int $id)
    {
        if ($this->getAttribute(self::ID) > 0) {
            return;
        }
        $this->attributes[self::ID] = $id;
    }
}
