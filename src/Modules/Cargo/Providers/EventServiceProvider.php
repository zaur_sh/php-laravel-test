<?php
declare(strict_types=1);

namespace Modules\Cargo\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Cargo\Events\CargoCreatedEvent;
use Modules\Cargo\Events\CargoUpdatedEvent;
use Modules\Cargo\Listeners\OnCargoCreatedSendNotification;
use Modules\Cargo\Listeners\OnCargoUpdatedSofDelete;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        CargoCreatedEvent::class => [
            OnCargoCreatedSendNotification::class,
        ],
        CargoUpdatedEvent::class => [
            OnCargoUpdatedSofDelete::class,
        ],
    ];
}
