<?php
declare(strict_types=1);

namespace Modules\Cargo\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Modules\Cargo\Models\Cargo;

class RouteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Route::model('cargo', Cargo::class);

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(__DIR__.'/../Routes/api.php');
        });
    }
}
