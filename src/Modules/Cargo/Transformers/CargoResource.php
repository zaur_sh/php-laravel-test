<?php
declare(strict_types=1);

namespace Modules\Cargo\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Cargo\Models\Cargo;

class CargoResource extends JsonResource
{
    private Cargo $cargo;

    public function __construct(Cargo $cargo)
    {
        parent::__construct($cargo);
        $this->cargo = $cargo;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->cargo->getAttribute(Cargo::ID),
            'weight' => $this->cargo->getAttribute(Cargo::WEIGHT),
            'volume' => $this->cargo->getAttribute(Cargo::VOLUME),
            'truck' => $this->cargo->getAttribute(Cargo::TRUCK),
        ];
    }
}
