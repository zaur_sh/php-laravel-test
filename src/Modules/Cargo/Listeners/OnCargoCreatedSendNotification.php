<?php
declare(strict_types=1);

namespace Modules\Cargo\Listeners;

use Modules\Cargo\Events\CargoCreatedEvent;

class OnCargoCreatedSendNotification
{
    public function __construct()
    {
        //
    }

    public function handle(CargoCreatedEvent $event)
    {
        //
    }
}
