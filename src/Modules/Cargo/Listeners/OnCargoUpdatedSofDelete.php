<?php
declare(strict_types=1);

namespace Modules\Cargo\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Cargo\Events\CargoUpdatedEvent;
use Modules\Cargo\Models\Cargo;

class OnCargoUpdatedSofDelete implements ShouldQueue
{
    public int $delay = 60 * 5;

    public function handle(CargoUpdatedEvent $event)
    {
        Cargo::withoutEvents(fn () => $event->cargo->delete());
    }
}
