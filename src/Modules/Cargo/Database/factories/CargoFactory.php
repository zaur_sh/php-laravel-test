<?php
declare(strict_types=1);

namespace Modules\Cargo\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Api\DTO\TruckDTO;
use Modules\Cargo\Models\Cargo;

class CargoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cargo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            Cargo::ID => $this->faker->randomDigitNotZero(),
            Cargo::WEIGHT => $this->faker->randomDigitNotZero(),
            Cargo::VOLUME => $this->faker->randomDigitNotZero(),
            Cargo::TRUCK => new TruckDTO(),
        ];
    }
}
