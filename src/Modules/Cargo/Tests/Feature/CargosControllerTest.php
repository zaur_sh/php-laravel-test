<?php

namespace Modules\Cargo\Tests\Feature;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Cargo\Models\Cargo;
use Tests\TestCase;

class CargosControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex()
    {
        Cargo::factory()->create();

        $response = $this->json('GET', '/api/cargos');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'meta' => [
                'offset',
                'limit',
            ],
            'data' => [
                '*' => [
                    'id',
                    'weight',
                    'volume',
                    'truck' => [
                        'quantity',
                        'belt_count',
                        'place_count',
                        'pallet_count',
                    ]
                ]
            ],
        ]);
    }

    /**
     * @dataProvider provider
     */
    public function testStore(array $data)
    {
        $response = $this->json('POST', '/api/cargos', $data);
        $response->assertExactJson([
            'data' => $data,
        ]);
        $response->assertStatus(200);
    }

    /**
     * @dataProvider provider
     */
    public function testUpdate(array $data)
    {
        $cargo = new Cargo($data);
        $cargo->save();

        $data[Cargo::VOLUME]++;
        $response = $this->json('PATCH', "/api/cargos/{$data[Cargo::ID]}", $data);
        $response->assertExactJson([
            'data' => $data,
        ]);
        $response->assertStatus(200);
    }

    /**
     * @dataProvider provider
     */
    public function testShow(array $data)
    {
        $cargo = new Cargo($data);
        $cargo->save();
        $response = $this->json('GET', "/api/cargos/{$data[Cargo::ID]}", $data);
        $response->assertExactJson([
            'data' => $data,
        ]);
        $response->assertStatus(200);
    }

    public function provider(): array
    {
        return [
            [
                [
                    Cargo::ID => 1,
                    Cargo::WEIGHT => 2,
                    Cargo::VOLUME => 3,
                    Cargo::TRUCK => [
                        'quantity' => 4,
                        'belt_count' => 5,
                        'place_count' => 6,
                        'pallet_count' => 7,
                    ],
                ]
            ]
        ];
    }
}
