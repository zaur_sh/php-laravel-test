<?php
declare(strict_types=1);

namespace Modules\Cargo\Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Modules\Api\DTO\CargoDTO;
use Modules\Cargo\Events\CargoCreatedEvent;
use Modules\Cargo\Events\CargoUpdatedEvent;
use Modules\Cargo\Jobs\CargoSyncJob;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use Tests\TestCase;

class CargoSyncJobTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *
     * @return void
     * @throws UnknownProperties
     */
    public function test()
    {
        $cargoDTO = new CargoDTO([
            'id' => 1,
            'weight' => 2,
            'volume' => 3,
            'truck' => [
                'quantity' => 4,
                'belt_count' => 5,
                'place_count' => 6,
                'pallet_count' => 7,
            ],
        ]);
        $cargoSyncJob = new CargoSyncJob($cargoDTO);

        Event::fake();
        $cargoSyncJob->handle();
        Event::assertDispatched(CargoCreatedEvent::class);
        Event::assertNotDispatched(CargoUpdatedEvent::class);

        Event::fake();
        $cargoDTO->weight++;
        $cargoSyncJob->handle();
        Event::assertNotDispatched(CargoCreatedEvent::class);
        Event::assertDispatched(CargoUpdatedEvent::class);
    }
}
