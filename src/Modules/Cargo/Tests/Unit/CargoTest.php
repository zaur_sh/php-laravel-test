<?php
declare(strict_types=1);

namespace Modules\Cargo\Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Api\DTO\CargoDTO;
use Modules\Cargo\Models\Cargo;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use Tests\TestCase;

class CargoTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *
     * @return void
     * @throws UnknownProperties
     */
    public function testNewFromDTO()
    {
        $cargoData = [
            'id' => 1,
            'weight' => 2,
            'volume' => 3,
            'truck' => [
                'quantity' => 4,
                'belt_count' => 5,
                'place_count' => 6,
                'pallet_count' => 7,
            ],
        ];
        $cargoDTO = new CargoDTO($cargoData);
        $cargo = Cargo::newFromDTO($cargoDTO);
        $this->assertEquals(
            $cargoData,
            $cargo->setVisible($cargo->getFillable())->toArray()
        );
    }
}
