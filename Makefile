.PHONY: install
install: setup_env composer_install generate_key


.PHONY: setup_env
setup_env:
	cp -n ./src/.env.docker.example ./src/.env


.PHONY: generate_key
generate_key:
	cd ./src \
	&& ./artisan key:generate --ansi


.PHONY: composer_install
composer_install:
	docker run --rm --interactive --tty \
		--volume ${PWD}/src:/app \
		--env COMPOSER_HOME \
		--env COMPOSER_CACHE_DIR \
		--volume ${COMPOSER_HOME:-$HOME/.config/composer}:$COMPOSER_HOME \
		--volume ${COMPOSER_CACHE_DIR:-$HOME/.cache/composer}:$COMPOSER_CACHE_DIR \
		--user $(id -u):$(id -g) \
		composer:2.1.3 install


.PHONY: start
start:
	USER=$(shell id -u) \
	GROUP=$(shell id -g) \
	docker-compose up


.PHONY: stop
stop:
	docker-compose down
